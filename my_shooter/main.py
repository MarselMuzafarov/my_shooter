import shooter


def main():
    game = shooter.Game()
    shooter.start_game(game)


if __name__ == "__main__":
    main()
