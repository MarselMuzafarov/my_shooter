### Shooter
RULES:

The player's task is to score as many points as possible.
Points are awarded for kills. If the enemy reaches the end of the line, then you have lost. 
If the enemy hits the hero (If the shield is not activated at this time), you have lost. 
Over time, the number of enemies will increase.There are zombies in the game with 1 and 3 health units
To start the game, just run the file "main.py '


# Control:

L_SHIFT - activate shield
SPACE - shot
W - upward movement
S - downward movement
ESC - pause
