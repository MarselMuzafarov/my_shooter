from random import choice
from models import *
from images import *


pygame.init()

file = open('absolute_record.txt', 'r')
start_absolute_record = int(file.read())
file.close()


class Game():

    def __init__(self):
        pygame.display.set_caption('Shooter')
        pygame.display.set_icon(dict_with_static_img['icon'])
        self.win = pygame.display.set_mode((1200, 705))
        self.hero = Hero(60, 450, 102, 91)
        self.bullets = []
        self.previously_difficult_score = 0
        self.score = 0
        self.run = True
        self.enemy_bullets = []
        self.enemies = []
        self.current_move = 0
        #variables starting with "cull down" are needed to control the speed of execution of something 
        #(speed of shots, speed of animation change, etc.)
        self.cull_down_for_born_enemy = 0
        self.cull_down_for_change_animation = 2
        self.cull_down_for_enemy_shot = 50
        self.cull_down_for_born_hard_enemy = 5
        self.max_count_of_enemy = 4
        self.height_of_path = (7, 100, 187, 282, 375, 465)
        self.distance_between_hero_and_bullet = 0
        self.absolute_record = start_absolute_record
        self.current_enemy_animation = moves[0]
        self.menu = True
        self.paused = False
        self.rules_page = False
        self.hero_skin = 1


def menu(game):
    record_a_record(game)
    nulling(game)
    button = Button(250, 49, 480, 270)
    button2 = Button(250, 49, 480, 325)
    button3 = Button(170, 170, 172, 400)
    button4 = Button(170, 170, 514, 400)
    button5 = Button(170, 170, 856, 400)
    while game.menu:
        game.win.blit(dict_with_static_img['bg_for_menu'], (0, 0))
        position = pygame.mouse.get_pos()
        pressed = pygame.mouse.get_pressed()
        draw_with_backlight(game, position, button, 'START!', dict_with_static_img['active_button_image'], dict_with_static_img['passive_button_image'])
        draw_with_backlight(game, position, button2, 'RULES AND CONTROL', dict_with_static_img['active_button_image'], dict_with_static_img['passive_button_image'])
        draw_with_backlight(game, position, button3, '', dict_with_static_img['big_button_active'], dict_with_static_img['big_button'])
        game.win.blit(dict_with_static_img['hero_img'], (177, 420))
        draw_with_backlight(game, position, button4, '', dict_with_static_img['big_button_active'], dict_with_static_img['big_button'])
        game.win.blit(dict_with_static_img['hero2_img'], (544, 420))
        draw_with_backlight(game, position, button5, '', dict_with_static_img['big_button_active'], dict_with_static_img['big_button'])
        game.win.blit(dict_with_static_img['hero3_img'], (886, 420))
        if  button3.x <= position[0] <= button3.x + button3.width and button3.y <= position[1] <= button3.y + button3.height and pressed[0] == 1:
            game.hero_skin = 1
        if  button4.x <= position[0] <= button4.x + button4.width and button4.y <= position[1] <= button4.y + button4.height and pressed[0] == 1:
            game.hero_skin = 2
        if  button5.x <= position[0] <= button5.x + button5.width and button5.y <= position[1] <= button5.y + button5.height and pressed[0] == 1:
            game.hero_skin = 3

        if game.hero_skin == 1:
            print_text(game, '*', button3.x + 15, button3.y + 5, font_color=(255, 0, 0))
        elif game.hero_skin == 2:
            print_text(game, '*', button4.x + 15, button4.y + 5, font_color=(255, 0, 0))
        elif game.hero_skin == 3:
            print_text(game, '*', button5.x + 15, button5.y + 5, font_color=(255, 0, 0))
        pygame.display.update()
        if  button.x <= position[0] <= button.x + button.width and button.y <= position[1] <= button.y + button.height and pressed[0] == 1:
            game.menu = False
        if  button2.x <= position[0] <= button2.x + button2.width and button2.y <= position[1] <= button2.y + button2.height and pressed[0] == 1:
            game.rules_page = True
            game.menu = False
        possible_quit(game)
    if game.rules_page:
        rule_page(game)


def rule_page(game):
    button3 = Button(250, 49, 5, 5)
    while game.rules_page:
        game.win.blit(dict_with_static_img['bg_for_rule_page'], (0, 0))
        position = pygame.mouse.get_pos()
        pressed = pygame.mouse.get_pressed()
        x = 10
        y = 100
        file = open('rules_and_control.txt')
        for line in file:
            print_text(game, line, x, y, font_color=(0,0,0)) 
            y += 30
        file.close() 
        draw_with_backlight(game, position, button3, 'BACK',  dict_with_static_img['active_button_image'], dict_with_static_img['passive_button_image'])
        if  button3.x <= position[0] <= button3.x + button3.width and button3.y <= position[1] <= button3.y + button3.height and pressed[0] == 1:
            game.menu = True
            game.rules_page = False
        pygame.display.update()
        possible_quit(game)
    if game.menu:    
        menu(game)

#buttons
def draw_with_backlight(game, mouse, button, message, active_img, passive_img):
    if button.x < mouse[0] < button.x + button.width and button.y < mouse[1] < button.y + button.height:
        game.win.blit(active_img, (button.x, button.y))
    else:
        game.win.blit(passive_img, (button.x, button.y))
    print_text(game, message, button.x + 15, button.y + 10, font_color=(0, 0, 0))

#reading keys and moving the main character
def keystrokes(game, keys):
    if keys[pygame.K_ESCAPE] and not game.menu and not game.rules_page and not game.paused:
        game.paused = True
        lose = False
        pause(game, lose)

    # hero shots
    if not keys[pygame.K_LSHIFT]:
        if keys[pygame.K_SPACE]:
            if len(game.bullets) < 20 and game.distance_between_hero_and_bullet < 0:
                if game.hero_skin == 1:
                    game.bullets.append(Projectile(game.hero.y + 40, game.hero.x + game.hero.width + 44, 5))
                elif game.hero_skin == 2 or game.hero_skin == 3:
                    game.bullets.append(Projectile(game.hero.y + 24, game.hero.x + game.hero.width - 10, 5))
                game.distance_between_hero_and_bullet = 10
            else:
                game.distance_between_hero_and_bullet -= 3

    #hero moves
    if not game.hero.is_jump and not game.hero.is_descent:
        if keys[pygame.K_w] and game.hero.y > 10:
            game.hero.is_jump = True
        if keys[pygame.K_s] and game.hero.y < 400:
            game.hero.is_descent = True
    elif game.hero.is_jump:
        if game.hero.long_of_move >= 0:
            game.hero.y -= game.hero.long_of_move**2
            game.hero.long_of_move -= 1
        else:
            game.hero.long_of_move = 6
            game.hero.is_jump = False
    elif game.hero.is_descent:
        if game.hero.long_of_move >= 0:
            game.hero.y += game.hero.long_of_move**2
            game.hero.long_of_move -= 1
        else:
            game.hero.long_of_move = 6
            game.hero.is_descent = False


def enemy_shot(game):
    if game.cull_down_for_enemy_shot <= 0:
        shoting_enemy = random.choice(game.enemies)
        game.enemy_bullets.append(Projectile(shoting_enemy.y + 35, shoting_enemy.x, 2))
        game.cull_down_for_enemy_shot = 50
    else:
        game.cull_down_for_enemy_shot -= 1


def record_a_record(game):
    if game.score >= game.absolute_record:
        file = open('absolute_record.txt', 'w')
        file.write(str(game.absolute_record))
        file.close()

#function to reset the game, used when starting the game again or to exit the menu
def nulling(game):
    game.score = 0
    game.previously_difficult_score = 0
    game.enemy_bullets = []
    game.bullets = []
    game.enemies = []
    game.cull_down_for_enemy_shot = 50
    stop = False
    game.max_count_of_enemy = 4
    game.cull_down_for_born_enemy = 0

#This is a break during the actual gameplay
def pause(game, lose):
    button_continue = Button(250, 49, 500, 25)
    button_again = Button(250, 49, 500, 80)
    button_menu = Button(250, 49, 500, 135)

    while game.paused:
        position = pygame.mouse.get_pos()
        pressed = pygame.mouse.get_pressed()
        possible_quit(game)
        game.win.blit(dict_with_static_img['bg_menu_in_game'], (490, 10))
        draw_with_backlight(game, position, button_again,'Try again!',  dict_with_static_img['active_button_image'], dict_with_static_img['passive_button_image'])
        draw_with_backlight(game, position, button_menu, 'MENU',  dict_with_static_img['active_button_image'], dict_with_static_img['passive_button_image'])
        if  button_again.x <= position[0] <= button_again.x + button_again.width and button_again.y <= position[1] <= button_again.y + button_again.height and pressed[0] == 1:
            nulling(game)
            game.paused = False
        if  button_menu.x <= position[0] <= button_menu.x + button_menu.width and button_menu.y <= position[1] <= button_menu.y + button_menu.height and pressed[0] == 1:
            game.menu = True
            game.paused = False
        if lose == False:
            draw_with_backlight(game, position, button_continue, 'Continue',  dict_with_static_img['active_button_image'], dict_with_static_img['passive_button_image'])
            if  button_continue.x <= position[0] <= button_continue.x + button_continue.width and button_continue.y <= position[1] <= button_continue.y + button_continue.height and pressed[0] == 1:
                game.paused = False
        else:
            print_text(game, 'YOU ARE LOSE', 550, 20)
        pygame.display.update()

    if game.menu:
        menu(game)


def print_text(game, message, x, y, font_color=(255, 255, 255), font_type='font.ttf', font_size=30):
    font_type = pygame.font.Font(font_type, font_size)
    text = font_type.render(message, True, font_color)
    game.win.blit(text, (x, y))

#rendering most of the game objects
def paint(game, keys):
    game.win.blit(dict_with_static_img['bg'], (0, 0))
    if game.hero_skin == 1:
        game.win.blit(dict_with_static_img['hero_img'], (game.hero.x, game.hero.y))
    elif game.hero_skin == 2:
        game.win.blit(dict_with_static_img['hero2_img'], (game.hero.x, game.hero.y))
    elif game.hero_skin == 3:
        game.win.blit(dict_with_static_img['hero3_img'], (game.hero.x, game.hero.y))

    if keys[pygame.K_LSHIFT]:
        game.win.blit(dict_with_static_img['shield'], (game.hero.x - 10, game.hero.y - 20))
    for bullet in game.enemy_bullets:
        game.win.blit(dict_with_static_img['enemy_bullet'], (bullet.x, bullet.y))
    for bullet in game.bullets:
        if game.hero_skin == 1:
            game.win.blit(dict_with_static_img['hero_bullet_for_hero1'], (bullet.x, bullet.y))
        elif game.hero_skin == 2 or game.hero_skin == 3:
            game.win.blit(dict_with_static_img['bullet_for_hero1_and_hero2'], (bullet.x, bullet.y))
    for enemy in game.enemies:
        if enemy.kind == 'small':
            game.win.blit(dict_with_simple_zombie_moves[str(enemy.animation)], (enemy.x, enemy.y))
        else:
            game.win.blit(dict_with_hard_zombie_moves[str(enemy.animation)], (enemy.x, enemy.y))

    game.win.blit(dict_with_static_img['tile'], (5, 605))
    game.win.blit(dict_with_static_img['tile'], (5, 655))
    print_text(game, 'Score: ' + str(game.score), 15, 613)
    print_text(game, 'Absolutely record: ' + str(game.absolute_record), 15, 670, font_size=16)
    pygame.display.update()


def new_absolute_record(game):
    if game.score > game.absolute_record:
        game.absolute_record = game.score


def increasing_difficulty_level(game):
    if game.score > game.previously_difficult_score + 8:
        game.previously_difficult_score += 8
        game.max_count_of_enemy += 1
    

def possible_quit(game):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            record_a_record(game)
            game.run = False
            game.menu = False
            game.rules_page = False
            game.paused = False
            

def lose_scenery(game, keys):
    for bullet in game.enemy_bullets:
        if not keys[pygame.K_LSHIFT]:
            if game.hero.x - 5 <= bullet.x <= game.hero.x + game.hero.width - 20 and  game.hero.y <= bullet.y <= game.hero.y + game.hero.height or game.enemies[0].x < 170:
                record_a_record(game)
                game.paused = True
                lose = True
                pause(game, lose)
                break

        elif game.hero.x - 5 <= bullet.x <= game.hero.x + game.hero.width + 20 and  game.hero.y <= bullet.y <= game.hero.y + game.hero.height:
            game.enemy_bullets.pop(game.enemy_bullets.index(bullet))


def adding_and_walk_enemy(game):
    if len(game.enemies) < game.max_count_of_enemy:
        if game.cull_down_for_born_enemy <= 0:
            game.cull_down_for_born_enemy = 10
            game.height_where_enemy_will_be_spawn = choice(game.height_of_path)
            if game.cull_down_for_born_hard_enemy >= 5:
                health = 4
                kind = 'big'
                game.cull_down_for_born_hard_enemy = 0
            else:
                health = 1
                kind = 'small'
                game.cull_down_for_born_hard_enemy += 1

            game.enemies.append(Enemy(game.height_where_enemy_will_be_spawn, -1, health, kind))
        else:
            game.cull_down_for_born_enemy -= 1

    for i in range(len(game.enemies)):
        game.enemies[i].x += game.enemies[i].speed


def movement_and_removement_hero_bullets(game):
    for bullet in game.bullets:
        if bullet.x < 1200:
            bullet.x += bullet.speed
        else:
            game.bullets.pop(game.bullets.index(bullet))
        for enemy in game.enemies:
            if enemy.x + 10 <= bullet.x <= enemy.x + enemy.width and enemy.y <= bullet.y <= enemy.y + enemy.height:                
                game.bullets.pop(game.bullets.index(bullet))
                enemy.health -= 1
                if enemy.health == 0:
                    game.enemies.pop(game.enemies.index(enemy))
                    game.score += 1


def remove_enemy_bullets(game):
    for bullet in game.enemy_bullets:
        if bullet.x > 0:
            bullet.x -= bullet.speed
        else:
            game.enemy_bullets.remove(game.enemy_bullets[game.enemy_bullets.index(bullet)])  


def change_animation_for_enemy(game):
    for enemy in game.enemies:
        if enemy.animation < len(moves) - 1:
            if game.cull_down_for_change_animation >= 2:
                enemy.animation += 1
                game.cull_down_for_change_animation = 0
            else:
                game.cull_down_for_change_animation += 1
        else:
            enemy.animation = 0


def playing(game):
    keys = pygame.key.get_pressed()
    keystrokes(game, keys)
    adding_and_walk_enemy(game)
    movement_and_removement_hero_bullets(game)
    enemy_shot(game)
    remove_enemy_bullets(game)
    change_animation_for_enemy(game)
    lose_scenery(game, keys)
    increasing_difficulty_level(game)
    new_absolute_record(game)
    paint(game, keys)
    possible_quit(game)


def start_game(game):
    clock = pygame.time.Clock()
    while game.run:
        clock.tick(60)
        if game.menu:
            menu(game)
        if game.run:
            playing(game)
