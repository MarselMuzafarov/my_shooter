import pygame
import random

pygame.init()


class Enemy():
    
    def __init__(self, y, speed, health, kind):
        self.x = 1210
        self.y = y
        self.speed = speed
        self.height = 75
        self.width = 62
        self.health = health
        self.animation = 0
        self.kind = kind


class Projectile():

    def __init__(self, y, x, speed):
        self.x = x
        self.y = y
        self.speed = speed


class Hero():

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.long_of_move = 6
        self.is_jump = False
        self.is_descent = False


class Button():

    def __init__(self, width, height, x, y):
        self.width = width
        self.height = height
        self.x = x
        self.y = y
