import pygame 

images = {
    'tile' : pygame.image.load('img/tile.png'), 
    'icon' : pygame.image.load('img/icon.png'),
    'bg' : pygame.image.load('img/bg.png'),
    'bg_menu_in_game' : pygame.image.load('img/bg_menu.png'),
    'active_button_image' : pygame.image.load('img/active_button.png'),
    'passive_button_image' : pygame.image.load('img/passive_button.png'),
    'hero_img' : pygame.image.load('img/hero.png'),
    'hero_bullet_for_hero1' : pygame.image.load('img/bullet.png'),
    'shield' : pygame.image.load('img/shield.png'),
    'enemy_bullet' : pygame.image.load('img/enemy_bullet.png'),
    'bg_for_menu' : pygame.image.load('img/bg_for_menu.png'),
    'bg_for_rule_page' : pygame.image.load('img/bg_rule_page.png'),
    'bullet_for_hero1_and_hero2':pygame.image.load('img/hero_bullet2.png'),
    'hero2_img':pygame.image.load('img/hero2.png'),
    'hero3_img':pygame.image.load('img/hero3.png'),
    'big_button':pygame.image.load('img/passive_button_for_chose_skin.png'),
    'big_button_active':pygame.image.load('img/active_button_for_chose_skin.png'),
    }

moves = [pygame.image.load('img/Walk ({}).png'.format(i)) for i in range(1, 8)]
moves2 = [pygame.image.load('img/character_zombie_walk{}.png'.format(i)) for i in range(7)]


def convert_static(diction):
    for key in diction:
        img_rect = diction[key].get_rect()
        img_surface = pygame.Surface((img_rect.width, img_rect.height), pygame.SRCALPHA)
        img_surface.fill((0, 0, 0, 0))
        img_surface.blit(diction[key], img_rect)
        diction[key] = img_surface
    return diction


def convert_moves(lst):
    diction = {}
    for i in range(len(lst)):
        diction[str(i)] = lst[i]
    return diction

#The 2 functions written above are needed to convert images to surfaces, as well as to render the surface. 
#This is necessary for a smoother picture, pictures are rendered much slower than surfaces, which leads to freezes in the game.

dict_with_static_img = convert_static(images)
dict_with_simple_zombie_moves = convert_moves(moves)
dict_with_hard_zombie_moves = convert_moves(moves2)
